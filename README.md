# Docker Stats Monitoring

This project allows you to spin up a local version of [docker-stats-api](https://gitlab.com/sbenv/veroxis/docker-stats-api) accompanied by an provisioned grafana dashboard quickly.

It is mostly intended for developers to monitor their own machines and applications while developing.

## Getting started

```sh
git clone https://gitlab.com/sbenv/veroxis/docker-stats-monitoring.git
cd docker-stats-monitoring
# customize this file to change port mappings
cp docker-compose.override.yml.dist docker-compose.override.yml
docker compose up -d
```

| Description   | Address        |
| ------------- | -------------- |
| Grafana       | localhost:3000 |
| Prometheus    | localhost:9090 |
| Node Exporter | localhost:9100 |

## Resource Limits

The preconfigured resource limits will work fine while havin few containers running. If the limits have to be increased edit your `docker-compose.override.yml` and override the limits as you need.
